// Exponent Operator
let getCube = 2;

// Template Literals
console.log(`The cube of ${getCube} is ${getCube**3}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
let [address1,address2,address3,address4] = address;

console.log(`I live at ${address1} ${address2}, ${address3} ${address4}`);


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name,species,weight,measurement} = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((x) => {
 	console.log(x);
 });


// Javascript Classes

 class Dog {
 	constructor(name,age,breed){
 		this.name = name;
 		this.age = age;
 		this.breed = breed;
 	}
 }

 let Dog1 = new Dog("Frank",45,"Alien/Pug");
 let Dog2 = new Dog("Bach",15,"German Shepherd");
 

 console.log(Dog1);
 console.log(Dog2);

